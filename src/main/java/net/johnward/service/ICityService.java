package net.johnward.service;

import net.johnward.model.City;

import java.util.List;

public interface ICityService {

    List<City> findAll();
}